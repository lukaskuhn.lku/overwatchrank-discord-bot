const Discord = require('discord.js');


const REGION_EU = "eu";
const REGION_US = "us";
const REGION_ASIA = "asia";

const command = function(message, args) {
    console.log(args);
    const region = args.shift().toLowerCase();

    if(region == REGION_EU) message.client.region = REGION_EU;
    if(region == REGION_US) message.client.region = REGION_US;
    if(region == REGION_ASIA) message.client.region = REGION_ASIA;
}

module.exports = {
    command,
    REGION_ASIA,
    REGION_EU,
    REGION_US
}