const Discord = require('discord.js');
const request = require('request');

const command = function(message, args) {
    console.log(args);
    const playerTag = args.shift().toLowerCase();
    const playerTagAPI = playerTag.replace("#", "-");
    
    request('https://ow-api.com/v1/stats/pc/eu/' + playerTagAPI + '/complete', function (error, response, body) {
        if(error){ 
            console.log("Error: " + error);
            message.channel.send(`Player _${playerTag}_ could not be loaded.`)
        }else{
            var body = JSON.parse(body);
            if(body.error) {
                message.channel.send(`Error for **${playerTag}**: ${body.error}`)
            }else{
                console.log("Body " + body);
                message.channel.send(`🎮 _${playerTag}_ current elo rating: **${body.rating}**`);
            }
        }
    });
}

module.exports = {
    command
}