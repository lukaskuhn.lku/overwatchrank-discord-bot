const { Client, Collection } = require('discord.js');
const { Region } = require('./commands/owregion');
const { Plattform } = require('./commands/owplattform');

module.exports = class extends Client {
	constructor() {
		super({
			disableEveryone: true,
			disabledEvents: ['TYPING_START']
        });
        
        this.region = Region.REGION_EU;
        this.plattform = "pc";
    }
}